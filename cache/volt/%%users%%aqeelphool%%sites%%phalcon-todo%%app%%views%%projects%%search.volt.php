<?php echo $this->getContent(); ?>

<ul class="pager">
    <li class="previous">
        <?php echo $this->tag->linkTo(array('projects', '&larr; Go Back')); ?>
    </li>
    <li class="next">
        <?php echo $this->tag->linkTo(array('projects/new', 'Create projects')); ?>
    </li>
</ul>

<?php $v38600116301153498671iterated = false; ?><?php $v38600116301153498671iterator = $page->items; $v38600116301153498671incr = 0; $v38600116301153498671loop = new stdClass(); $v38600116301153498671loop->length = count($v38600116301153498671iterator); $v38600116301153498671loop->index = 1; $v38600116301153498671loop->index0 = 1; $v38600116301153498671loop->revindex = $v38600116301153498671loop->length; $v38600116301153498671loop->revindex0 = $v38600116301153498671loop->length - 1; ?><?php foreach ($v38600116301153498671iterator as $project) { ?><?php $v38600116301153498671loop->first = ($v38600116301153498671incr == 0); $v38600116301153498671loop->index = $v38600116301153498671incr + 1; $v38600116301153498671loop->index0 = $v38600116301153498671incr; $v38600116301153498671loop->revindex = $v38600116301153498671loop->length - $v38600116301153498671incr; $v38600116301153498671loop->revindex0 = $v38600116301153498671loop->length - ($v38600116301153498671incr + 1); $v38600116301153498671loop->last = ($v38600116301153498671incr == ($v38600116301153498671loop->length - 1)); ?><?php $v38600116301153498671iterated = true; ?>
    <?php if ($v38600116301153498671loop->first) { ?>
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th colspan=3>Description</th>
        </tr>
    </thead>
    <tbody>
    <?php } ?>
        <tr>
            <td><?php echo $project->id; ?></td>
            <td><?php echo $project->name; ?></td>
            <td><?php echo $project->description; ?></td>
            <td width="7%"><?php echo $this->tag->linkTo(array('projects/edit/' . $project->id, '<i class="glyphicon glyphicon-edit"></i> Edit', 'class' => 'btn btn-default')); ?></td>
            <td width="7%"><?php echo $this->tag->linkTo(array('projects/delete/' . $project->id, '<i class="glyphicon glyphicon-remove"></i> Delete', 'class' => 'btn btn-default')); ?></td>
        </tr>
    <?php if ($v38600116301153498671loop->last) { ?>
    </tbody>
    <tbody>
        <tr>
            <td colspan="7" align="right">
                <div class="btn-group">
                    <?php echo $this->tag->linkTo(array('projects/search', '<i class="icon-fast-backward"></i> First', 'class' => 'btn')); ?>
                    <?php echo $this->tag->linkTo(array('projects/search?page=' . $page->before, '<i class="icon-step-backward"></i> Previous', 'class' => 'btn')); ?>
                    <?php echo $this->tag->linkTo(array('projects/search?page=' . $page->next, '<i class="icon-step-forward"></i> Next', 'class' => 'btn')); ?>
                    <?php echo $this->tag->linkTo(array('projects/search?page=' . $page->last, '<i class="icon-fast-forward"></i> Last', 'class' => 'btn')); ?>
                    <span class="help-inline"><?php echo $page->current; ?> of <?php echo $page->total_pages; ?></span>
                </div>
            </td>
        </tr>
    </tbody>
</table>
    <?php } ?>
<?php $v38600116301153498671incr++; } if (!$v38600116301153498671iterated) { ?>
    No products are recorded
<?php } ?>
