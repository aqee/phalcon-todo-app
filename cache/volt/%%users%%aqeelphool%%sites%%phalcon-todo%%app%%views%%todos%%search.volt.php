<?php echo $this->getContent(); ?>

<ul class="pager">
    <li class="previous">
        <?php echo $this->tag->linkTo(array('todos', '&larr; Go Back')); ?>
    </li>
    <li class="next">
        <?php echo $this->tag->linkTo(array('todos/new', 'Create Todos')); ?>
    </li>
</ul>

<?php $v56125488518806944421iterated = false; ?><?php $v56125488518806944421iterator = $page->items; $v56125488518806944421incr = 0; $v56125488518806944421loop = new stdClass(); $v56125488518806944421loop->length = count($v56125488518806944421iterator); $v56125488518806944421loop->index = 1; $v56125488518806944421loop->index0 = 1; $v56125488518806944421loop->revindex = $v56125488518806944421loop->length; $v56125488518806944421loop->revindex0 = $v56125488518806944421loop->length - 1; ?><?php foreach ($v56125488518806944421iterator as $todo) { ?><?php $v56125488518806944421loop->first = ($v56125488518806944421incr == 0); $v56125488518806944421loop->index = $v56125488518806944421incr + 1; $v56125488518806944421loop->index0 = $v56125488518806944421incr; $v56125488518806944421loop->revindex = $v56125488518806944421loop->length - $v56125488518806944421incr; $v56125488518806944421loop->revindex0 = $v56125488518806944421loop->length - ($v56125488518806944421incr + 1); $v56125488518806944421loop->last = ($v56125488518806944421incr == ($v56125488518806944421loop->length - 1)); ?><?php $v56125488518806944421iterated = true; ?>
    <?php if ($v56125488518806944421loop->first) { ?>
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Todo Title</th>
            <th>Description</th>
            <th>Project</th>
            <th colspan=3>Status</th>
        </tr>
    </thead>
    <tbody>
    <?php } ?>
        <tr>
            <td><?php echo $todo->id; ?></td>
            <td><?php echo $todo->title; ?></td>
            <td><?php echo $todo->description; ?></td>
            <td><?php echo $todo->getProjects()->name; ?></td>
            <td><?php echo $todo->status; ?></td>
            <td width="7%"><?php echo $this->tag->linkTo(array('todos/edit/' . $todo->id, '<i class="glyphicon glyphicon-edit"></i> Edit', 'class' => 'btn btn-default')); ?></td>
            <td width="7%"><?php echo $this->tag->linkTo(array('todos/delete/' . $todo->id, '<i class="glyphicon glyphicon-remove"></i> Delete', 'class' => 'btn btn-default')); ?></td>
        </tr>
    <?php if ($v56125488518806944421loop->last) { ?>
    </tbody>
    <tbody>
        <tr>
            <td colspan="7" align="right">
                <div class="btn-group">
                    <?php echo $this->tag->linkTo(array('todos/search', '<i class="icon-fast-backward"></i> First', 'class' => 'btn')); ?>
                    <?php echo $this->tag->linkTo(array('todos/search?page=' . $page->before, '<i class="icon-step-backward"></i> Previous', 'class' => 'btn')); ?>
                    <?php echo $this->tag->linkTo(array('todos/search?page=' . $page->next, '<i class="icon-step-forward"></i> Next', 'class' => 'btn')); ?>
                    <?php echo $this->tag->linkTo(array('todos/search?page=' . $page->last, '<i class="icon-fast-forward"></i> Last', 'class' => 'btn')); ?>
                    <span class="help-inline"><?php echo $page->current; ?> of <?php echo $page->total_pages; ?></span>
                </div>
            </td>
        </tr>
    </tbody>
</table>
    <?php } ?>
<?php $v56125488518806944421incr++; } if (!$v56125488518806944421iterated) { ?>
    No todos are recorded
<?php } ?>
