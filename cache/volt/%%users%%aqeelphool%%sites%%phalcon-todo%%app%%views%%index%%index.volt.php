
<?php echo $this->getContent(); ?>

<div class="jumbotron">
    <h1>Phalcon Todo App</h1>
    <p>This is a simple todos management application written in Phalcon Framework. The objective of this appliction is to get through some of the basic features Phalcon provides.</p>
    <?php if ($this->session->get('auth') == null) { ?>
    <p><?php echo $this->tag->linkTo(array('session/index', 'Login&raquo;', 'class' => 'btn btn-primary btn-large')); ?>  <?php echo $this->tag->linkTo(array('register', 'Register Now &raquo;', 'class' => 'btn btn-primary btn-large btn-success')); ?></p>
    <?php } else { ?>
    <p><?php echo $this->tag->linkTo(array('todos/index', 'Browse your todos&raquo;', 'class' => 'btn btn-primary btn-large btn-success')); ?></p>
    <?php } ?>
</div>
