Phalcon TODO Application
================

This is a basic todo app written in Phalcon framework. It primarily covers the following areas of Phalcon:

- Bootstrap
- Basic Application Structure & MVC
- Forms
- Plugins
- Ini based configurations
- Volt Templates

Thanks.
Aqeel

Improvements for the future
-----------

- Add status, assign_to & due_on to todo crud as those are already added to the schema
- Implement CRUD using RESTful services
- Project Dropdown in todos search is showing projects from all the users, need to restrict to logged in users projects only


Required version: >= 2.0.0

Get Started
-----------

#### Requirements

To run this application on your machine, you need at least:

* >= PHP 5.3.9
* Apache Web Server with mod rewrite enabled
* Latest Phalcon Framework extension installed/enabled

Then you'll need to create the database and initialize schema:

    echo 'CREATE DATABASE phalcon_todo' | mysql -u root
    cat schemas/todos.sql | mysql -u root phalcon_todo
