{{ content() }}

<ul class="pager">
    <li class="previous">
        {{ link_to("projects", "&larr; Go Back") }}
    </li>
    <li class="next">
        {{ link_to("projects/new", "Create projects") }}
    </li>
</ul>

{% for project in page.items %}
    {% if loop.first %}
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th colspan=3>Description</th>
        </tr>
    </thead>
    <tbody>
    {% endif %}
        <tr>
            <td>{{ project.id }}</td>
            <td>{{ project.name }}</td>
            <td>{{ project.description }}</td>
            <td width="7%">{{ link_to("projects/edit/" ~ project.id, '<i class="glyphicon glyphicon-edit"></i> Edit', "class": "btn btn-default") }}</td>
            <td width="7%">{{ link_to("projects/delete/" ~ project.id, '<i class="glyphicon glyphicon-remove"></i> Delete', "class": "btn btn-default") }}</td>
        </tr>
    {% if loop.last %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="7" align="right">
                <div class="btn-group">
                    {{ link_to("projects/search", '<i class="icon-fast-backward"></i> First', "class": "btn") }}
                    {{ link_to("projects/search?page=" ~ page.before, '<i class="icon-step-backward"></i> Previous', "class": "btn") }}
                    {{ link_to("projects/search?page=" ~ page.next, '<i class="icon-step-forward"></i> Next', "class": "btn") }}
                    {{ link_to("projects/search?page=" ~ page.last, '<i class="icon-fast-forward"></i> Last', "class": "btn") }}
                    <span class="help-inline">{{ page.current }} of {{ page.total_pages }}</span>
                </div>
            </td>
        </tr>
    </tbody>
</table>
    {% endif %}
{% else %}
    No products are recorded
{% endfor %}
