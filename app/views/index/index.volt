
{{ content() }}

<div class="jumbotron">
    <h1>Phalcon Todo App</h1>
    <p>This is a simple todos management application written in Phalcon Framework. The objective of this appliction is to get through some of the basic features Phalcon provides.</p>
    {% if session.get('auth') == null %}
    <p>{{ link_to('session/index', 'Login&raquo;', 'class': 'btn btn-primary btn-large')}}  {{link_to('register', 'Register Now &raquo;', 'class': 'btn btn-primary btn-large btn-success') }}</p>
    {% else %}
    <p>{{ link_to('todos/index', 'Browse your todos&raquo;', 'class': 'btn btn-primary btn-large btn-success') }}</p>
    {% endif %}
</div>
