{{ content() }}

<ul class="pager">
    <li class="previous">
        {{ link_to("todos", "&larr; Go Back") }}
    </li>
    <li class="next">
        {{ link_to("todos/new", "Create Todos") }}
    </li>
</ul>

{% for todo in page.items %}
    {% if loop.first %}
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Todo Title</th>
            <th>Description</th>
            <th>Project</th>
            <th colspan=3>Status</th>
        </tr>
    </thead>
    <tbody>
    {% endif %}
        <tr>
            <td>{{ todo.id }}</td>
            <td>{{ todo.title }}</td>
            <td>{{ todo.description }}</td>
            <td>{{ todo.getProjects().name }}</td>
            <td>{{ todo.status }}</td>
            <td width="7%">{{ link_to("todos/edit/" ~ todo.id, '<i class="glyphicon glyphicon-edit"></i> Edit', "class": "btn btn-default") }}</td>
            <td width="7%">{{ link_to("todos/delete/" ~ todo.id, '<i class="glyphicon glyphicon-remove"></i> Delete', "class": "btn btn-default") }}</td>
        </tr>
    {% if loop.last %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="7" align="right">
                <div class="btn-group">
                    {{ link_to("todos/search", '<i class="icon-fast-backward"></i> First', "class": "btn") }}
                    {{ link_to("todos/search?page=" ~ page.before, '<i class="icon-step-backward"></i> Previous', "class": "btn") }}
                    {{ link_to("todos/search?page=" ~ page.next, '<i class="icon-step-forward"></i> Next', "class": "btn") }}
                    {{ link_to("todos/search?page=" ~ page.last, '<i class="icon-fast-forward"></i> Last', "class": "btn") }}
                    <span class="help-inline">{{ page.current }} of {{ page.total_pages }}</span>
                </div>
            </td>
        </tr>
    </tbody>
</table>
    {% endif %}
{% else %}
    No todos are recorded
{% endfor %}
