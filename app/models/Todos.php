<?php

use Phalcon\Mvc\Model;

/**
 * Todos
 */
class Todos extends Model
{
	/**
	 * @var integer
	 */
	public $id;

	/**
	 * @var integer
	 */
	public $project_id;

	/**
	 * @var string
	 */
	public $title;

	/**
	 * @var string
	 */
	public $description;

	/**
	 * @var string
	 */
	public $status;

	/**
	 * Todos initializer
	 */
	public function initialize()
	{
		$this->belongsTo('project_id', 'Projects', 'id', array(
			'reusable' => true
		));

		$this->belongsTo('created_by', 'Users', 'id', array(
            'reusable' => true
        ));
	}

}
