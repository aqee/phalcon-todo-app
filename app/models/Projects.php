<?php

use Phalcon\Mvc\Model;

/**
 * Projects
 */
class Projects extends Model
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;

    /**
     * @var integer
     */
    public $created_by;

    /**
     * Projects initializer
     */
    public function initialize()
    {
        $this->hasMany('id', 'Todos', 'project_id', array(
        	'foreignKey' => array(
        		'message' => 'Projects cannot be deleted because it\'s used in Todos'
        	)
        ));

        $this->belongsTo('created_by', 'Users', 'id', array(
            'reusable' => true
        ));
    }
}
