<?php

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

/**
 * ProjectsController
 *
 * Manage CRUD operations for Projects
 */
class ProjectsController extends ControllerBase
{
    private $current_user;
    public function initialize()
    {
        $this->tag->setTitle('Manage your Projects');
        parent::initialize();
        $this->current_user = $this->session->get('auth');
    }

    /**
     * Shows the index action
     */
    public function indexAction()
    {
        $this->session->conditions = null;
        $this->view->form = new ProjectsForm;
    }

    /**
     * Search Projects based on current criteria
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Projects", $this->request->getPost());
            $query->andWhere('created_by = '. $this->current_user['id']);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $projects = Projects::find($parameters);
        if (count($projects) == 0) {
            $this->flash->notice("The search did not find any projects");
            return $this->forward("projects/index");
        }

        $paginator = new Paginator(array(
            "data"  => $projects,
            "limit" => 10,
            "page"  => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Shows the form to create a new project
     */
    public function newAction()
    {
        $this->view->form = new ProjectsForm(null, array('edit' => true));
    }

    /**
     * Edits a project based on its id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $project = Projects::findFirstById($id);
            if (!$project) {
                $this->flash->error("project was not found");
                return $this->forward("Projects/index");
            }

            $this->view->form = new ProjectsForm($project, array('edit' => true));
        }
    }

    /**
     * Creates a new project
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->forward("projects/index");
        }

        $form = new ProjectsForm;
        $project = new Projects();

        $data = $this->request->getPost();
        if (!$form->isValid($data, $project)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('projects/new');
        }

        $current_user = $this->session->get('auth');

        $project->created_by = $current_user['id'];

        if ($project->save() == false) {
            foreach ($project->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('projects/new');
        }

        $form->clear();

        $this->flash->success("Project was created successfully");
        return $this->forward("projects/index");
    }

    /**
     * Saves current project in screen
     *
     * @param string $id
     */
    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->forward("projects/index");
        }

        $id = $this->request->getPost("id", "int");

        $project = Projects::findFirstById($id);
        if (!$project) {
            $this->flash->error("Project does not exist");
            return $this->forward("projects/index");
        }

        $form = new ProjectsForm;
        $this->view->form = $form;

        $data = $this->request->getPost();

        if (!$form->isValid($data, $project)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('projects/edit/' . $id);
        }

        if ($project->save() == false) {
            foreach ($project->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('projects/edit/' . $id);
        }

        $form->clear();

        $this->flash->success("Project was updated successfully");
        return $this->forward("projects/index");
    }

    /**
     * Deletes a project
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $Projects = Projects::findFirstById($id);
        if (!$Projects) {
            $this->flash->error("Project was not found");
            return $this->forward("projects/index");
        }

        if (!$Projects->delete()) {
            foreach ($Projects->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward("projects/search");
        }

        $this->flash->success("Project was deleted");
        return $this->forward("projects/index");
    }
}
