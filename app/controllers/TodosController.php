<?php

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

/**
 * TodosController
 *
 * Manage CRUD operations for todos
 */
class TodosController extends ControllerBase
{
    private $current_user;
    public function initialize()
    {
        $this->tag->setTitle('Manage your Todos');
        parent::initialize();
        $this->current_user = $this->session->get('auth');
        
    }

    /**
     * index or default action for todos page, will show a search form by default
     */
    public function indexAction()
    {
        $this->session->conditions = null;
        $this->view->form = new TodosForm;
    }

    /**
     * Search todos based on current criteria
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Todos", $this->request->getPost());
            $query->andWhere('created_by = '. $this->current_user['id']);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $todos = Todos::find($parameters);
        if (count($todos) == 0) {
            $this->flash->notice("The search did not find any todos");
            return $this->forward("todos/index");
        }

        $paginator = new Paginator(array(
            "data"  => $todos,
            "limit" => 10,
            "page"  => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Shows the form to create a new todo
     */
    public function newAction()
    {
        $this->view->form = new TodosForm(null, array('edit' => true));
    }

    /**
     * Creates a new todo
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->forward("todos/index");
        }

        $form = new TodosForm;
        $todo = new Todos();

        $data = $this->request->getPost();
        if (!$form->isValid($data, $todo)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('todos/new');
        }

        $todo->created_by = $this->current_user['id'];

        if ($todo->save() == false) {
            foreach ($todo->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('todos/new');
        }

        $form->clear();

        $this->flash->success("Todo was created successfully");
        return $this->forward("todos/index");
    }

    /**
     * Deletes a todo
     *
     * @param integer $id
     */
    public function deleteAction($id)
    {

        $todos = Todos::findFirstById($id);
        if (!$todos) {
            $this->flash->error("Todo was not found");
            return $this->forward("todos/index");
        }

        if (!$todos->delete()) {
            foreach ($todos->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward("todos/search");
        }

        $this->flash->success("Todo was deleted");
        return $this->forward("todos/index");
    }

    /**
     * Edits a tood based on its id
     *
     * @param integer $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $todo = Todos::findFirstById($id);
            if (!$todo) {
                $this->flash->error("Todo was not found");
                return $this->forward("todos/index");
            }

            $this->view->form = new TodosForm($todo, array('edit' => true));
        }
    }

    /**
     * Saves current todo in screen
     *
     * @param string $id
     */
    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->forward("todos/index");
        }

        $id = $this->request->getPost("id", "int");

        $todo = Todos::findFirstById($id);
        if (!$todo) {
            $this->flash->error("Todo does not exist");
            return $this->forward("todos/index");
        }

        $form = new TodosForm;
        $this->view->form = $form;

        $data = $this->request->getPost();

        if (!$form->isValid($data, $todo)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('todos/edit/' . $id);
        }

        if ($todo->save() == false) {
            foreach ($product->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('todos/edit/' . $id);
        }

        $form->clear();

        $this->flash->success("Todo was updated successfully");
        return $this->forward("todos/index");
    }

}

